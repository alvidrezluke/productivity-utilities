#!/usr/bin/env node

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

// Import utilities
import alphabetize from './utilities/alphabetize.js';
import imageConverter from './utilities/image-converter.js';
import { pdfMerger, pdfSplitter } from './utilities/pdf-manipulator.js';

yargs(hideBin(process.argv))
  .command(
    'alphabetize [file]',
    'Alphabetize a file or will open a temporary file',
    (yargs) => {
      return yargs.positional('file', {
        describe: 'File to alphabetize',
        type: 'string',
        default: '',
      });
    },
    (argv) => {
      if (argv.verbose) console.info(`Alphabetizing ${argv.file}`);
      alphabetize(argv.file);
    }
  )
  .command(
    'imageconverter <file> <newType> [outputDirectory]',
    'Convert an image to a different type',
    (yargs) => {
      return yargs
        .positional('file', {
          describe: 'File to convert',
          type: 'string',
        })
        .positional('newType', {
          describe: 'New type of image',
          choices: ['jpg', 'png', 'bmp', 'webp'],
        })
        .positional('outputDirectory', {
          describe:
            'Output directory, the default directory is the tmp folder of this project',
          type: 'string',
        });
    },
    (argv) => {
      if (argv.verbose)
        console.info(`Converting ${argv.file} to ${argv.newType}`);
      imageConverter(argv.file, argv.newType, argv.outputDirectory);
    }
  )
  .command(
    'pdfmerger <file> <file>',
    'Merge multiple PDFs into one',
    (yargs) => {
      return yargs
        .positional('file', {
          describe: 'File to merge',
          type: 'string',
        })
        .option('outputDirectory', {
          alias: 'o',
          describe: 'Output directory',
          type: 'string',
        });
    },
    (argv) => {
      if (argv.verbose) console.info(`Merging PDFs`);
      let files = process.argv.slice(3);
      if (argv.outputDirectory) files = files.slice(0, -2);
      pdfMerger(files, argv.outputDirectory);
    }
  )
  .command(
    'pdfsplitter <file> <pagesToExtract>',
    'Extract pages from a PDF',
    (yargs) => {
      return yargs
        .positional('file', {
          describe: 'File to extract',
          type: 'string',
        })
        .positional('pagesToExtract', {
          describe:
            'Pages to extract. Use - for spans and , for individual pages',
          type: 'string',
        })
        .option('outputDirectory', {
          alias: 'o',
          describe: 'Output directory',
          type: 'string',
        });
    },
    (argv) => {
      if (argv.verbose) console.info(`Extracting PDFs`);
      pdfSplitter(argv.file, argv.pagesToExtract, argv.outputDirectory);
    }
  )
  .option('verbose', {
    alias: 'v',
    type: 'boolean',
    description: 'Run with verbose logging',
  })
  .parse();
