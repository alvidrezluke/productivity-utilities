import chalk from 'chalk';

const errorMessage = (text) => console.log(chalk.red(text));

const successMessage = (text) => console.log(chalk.green(text));

const workingMessage = (text) => console.log(chalk.yellow(text));

export { errorMessage, successMessage, workingMessage };
