import Jimp from 'jimp';
import path from 'path';

// Import helpers
import {
  errorMessage,
  successMessage,
  workingMessage,
} from '../helpers/text-messages.js';

const imageConverter = (file, newType, outputDirectory) => {
  const fileName = new URL(file).pathname.split('\\').pop().split('.')[0];
  Jimp.read(file, (err, image) => {
    if (err) {
      errorMessage(`Could not read image: ${err}`);
      return;
    }
    workingMessage('Converting image...');

    if (outputDirectory) {
      image.write(path.join(outputDirectory, `${fileName}.${newType}`));
      successMessage('Image converted and saved to:');
      console.log('\t', outputDirectoryURL);
    } else {
      image.write(path.join('tmp/converted-images/', `${fileName}.${newType}`));
      successMessage('Image converted and saved to:');
      console.log(
        '\t',
        path.join(
          import.meta.url
            .replace('/bin/utilities/image-converter.js', '')
            .replace('file:///', ''),
          'tmp/converted-images/',
          `${fileName}.${newType}`
        )
      );
    }
  });
};

export default imageConverter;
