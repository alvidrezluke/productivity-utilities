import PDFMerger from 'pdf-merger-js';
import path from 'path';

// Import helpers
import {
  errorMessage,
  successMessage,
  workingMessage,
} from '../helpers/text-messages.js';

const pdfMerger = async (files, outputDirectory) => {
  const merger = new PDFMerger();
  files.forEach((file) => {
    merger.add(path.join(file));
  });
  workingMessage(`Merging ${files.length} files`);
  if (outputDirectory) {
    merger.save(path.join(outputDirectory, 'merged.pdf'));
    successMessage('Sucessfully merged files and saved to: ');
    console.log('\t', path.join(outputDirectory, 'merged.pdf'));
  } else {
    merger.save('tmp/merged.pdf');
    successMessage('Sucessfully merged files and saved to: ');
    console.log(
      '\t',
      path.join(
        import.meta.url
          .replace('/bin/utilities/pdf-manipulator.js', '')
          .replace('file:///', ''),
        'tmp/merged.pdf'
      )
    );
  }
};

const pdfSplitter = async (file, pagesToExtract, outputDirectory) => {
  const splitter = new PDFMerger();
  if (pagesToExtract.indexOf(',') > -1) {
    splitter.add(path.join(file), pagesToExtract);
  } else if (pagesToExtract.indexOf('-') > -1) {
    splitter.add(path.join(file), pagesToExtract);
  } else if (pagesToExtract) {
    splitter.add(path.join(file), [pagesToExtract]);
  } else {
    errorMessage('Please provide a valid page number. Example: 1,2,3,4-6');
    return;
  }

  workingMessage(`Extracting page(s) ${pagesToExtract} from ${file}`);

  if (outputDirectory) {
    splitter.save(path.join(outputDirectory, 'split.pdf'));
    successMessage('Successfully split files and saved to: ');
    console.log('\t', path.join(outputDirectory, 'split.pdf'));
  } else {
    splitter.save('tmp/split.pdf');
    successMessage('Successfully split files and saved to: ');
    console.log(
      '\t',
      path.join(
        import.meta.url
          .replace('file:///', '')
          .replace('/bin/utilities/pdf-manipulator.js'),
        'tmp/split.pdf'
      )
    );
  }
};

export { pdfMerger, pdfSplitter };
