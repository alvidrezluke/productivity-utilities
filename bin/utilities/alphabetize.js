import editor from 'editor';
import fs from 'fs';

// Import helpers
import {
  errorMessage,
  successMessage,
  workingMessage,
} from '../helpers/text-messages.js';

const alphabetize = (file) => {
  if (file.toString().length > 0) {
    let alphabetizedFile = fs
      .readFileSync(file.toString())
      .toString()
      .toLowerCase()
      .split('\n')
      .sort();
    alphabetizedFile = alphabetizedFile.join('\n');
    console.log(
      '\n-------------------------------------\n' +
        alphabetizedFile +
        '\n-------------------------------------\n'
    );
    fs.writeFileSync('tmp/last-sort.txt', alphabetizedFile);
    successMessage('Temporarily saved to (will be overwritten on next sort):');
    console.log(
      '\t' +
        new URL(
          './tmp/last-sort.txt',
          import.meta.url.replace('/bin/utilities', '')
        ).href.replace('file:///', '')
    );
  } else {
    workingMessage('No file specified. Creating a temporary file.\n');
    const tempFilePath = 'tmp/tmp.txt';
    if (fs.existsSync(tempFilePath)) {
      errorMessage(
        'Could not create temp file. Make sure all instances of "temp.txt" are closed.'
      );
      return;
    } else {
      fs.writeFileSync(tempFilePath, '');
    }
    editor(tempFilePath, (code, sig) => {
      if (code === 0) {
        let alphabetizedFile = fs
          .readFileSync(tempFilePath)
          .toString()
          .toLowerCase()
          .split('\n')
          .sort();
        alphabetizedFile = alphabetizedFile.join('\n');
        console.log(
          '\n-------------------------------------\n' +
            alphabetizedFile +
            '\n-------------------------------------\n'
        );
        fs.writeFileSync('tmp/last-sort.txt', alphabetizedFile);
        successMessage(
          'Temporarily saved to (will be overwritten on next sort):'
        );
        console.log(
          '\t' +
            new URL(
              './tmp/last-sort.txt',
              import.meta.url.replace('/bin/utilities', '')
            ).href.replace('file:///', '')
        );
        fs.unlinkSync(tempFilePath);
      }
    });
  }
};

export default alphabetize;
