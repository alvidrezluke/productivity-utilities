### About productivity-utilities

Productivity utilities is a series of utilities that run on Node.JS.
These are created whenever I find something that can be automated efficiently.
Install globally and then use --help on the command to list all of the tools currently created.

Please contribute new tools or ideas.
